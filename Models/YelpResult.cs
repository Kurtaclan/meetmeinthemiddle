﻿namespace Models
{
    public class YelpResult
    {
        public string ImageUrl { get; set; }
        public double Rating { get; set; }
        public string Address { get; set; }
        public string FullAddress { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string ReviewCount { get; set; }
        public string YelpId { get; set; }
    }
}