﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class YelpQuery
    {
        public string Term { get; set; }
        public string Rating { get; set; }
        public string Price { get; set; }
        public string Categories { get; set; }
        public string SW_Latitude { get; set; }
        public string SW_Longitude { get; set; }
        public string NE_Latitude { get; set; }
        public string NE_Longitude { get; set; }
    }
}
