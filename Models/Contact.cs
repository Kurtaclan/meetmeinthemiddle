﻿namespace Models
{
    public class Contact
    {
        public string UserName { get; set; }
        public string HomeAddress { get; set; }
        public string DayAddress { get; set; }
    }
}
