﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class YelpSearchResult
    {
        public Business[] businesses { get; set; }

        public YelpResult[] GetYelpResults()
        {
            return businesses.Select(x => new YelpResult()
            {
                Address = String.Join(", ", x.location.address),
                FullAddress = String.Join(", ", x.location.display_address),
                ImageUrl = x.image_url,
                Name = x.name,
                PhoneNumber = x.phone,
                Rating = x.rating,
                YelpId = x.id,
                ReviewCount = x.review_count
            }).ToArray();
        }
    }

    public class Business
    {
        public double rating { get; set; }
        public string image_url { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string id { get; set; }
        public string review_count { get; set; }
        public Location location { get; set; }
    }

    public class Location
    {
        public string[] address { get; set; }
        public string[] display_address { get; set; }
    }
}
