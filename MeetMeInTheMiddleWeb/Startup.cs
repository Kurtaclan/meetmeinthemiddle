﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MeetMeInTheMiddleWeb.Startup))]
namespace MeetMeInTheMiddleWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
