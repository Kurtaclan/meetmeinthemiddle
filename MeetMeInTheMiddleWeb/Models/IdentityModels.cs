﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MoreLinq;

namespace MeetMeInTheMiddleWeb.Models
{
    public class Address
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long AddressId { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }

    public class Friend
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long FriendId { get; set; }
        public string UserId { get; set; }
        public string FriendUserId { get; set; }
        public virtual ApplicationUser User { get; set; }        
        public virtual ApplicationUser FriendUser { get; set; }
        public bool Confirmed { get; set; }
    }

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public virtual ICollection<Friend> Friends { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<Friend> ExternalFriends { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here

            return userIdentity;
        }

        public ApplicationUser GetUserByUserName(string userName)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Users
                    .Include(x => x.Addresses)
                    .Include(x => x.Friends)
                    .Include(x => x.ExternalFriends)
                    .Include(x => x.Friends.Select(y => y.FriendUser))
                    .Include(x => x.Friends.Select(y => y.FriendUser.Addresses))
                    .Include(x => x.ExternalFriends.Select(y => y.User))
                    .Include(x => x.ExternalFriends.Select(y => y.User.Addresses))
                    .FirstOrDefault(x => x.UserName == userName);
            }
        }

        public ApplicationUser GetUserByEmail(string email)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Users
                    .Include(x => x.Addresses)
                    .Include(x => x.Friends)
                    .Include(x => x.ExternalFriends)
                    .Include(x => x.Friends.Select(y => y.FriendUser))
                    .Include(x => x.Friends.Select(y => y.FriendUser.Addresses))
                    .Include(x => x.ExternalFriends.Select(y => y.User))
                    .Include(x => x.ExternalFriends.Select(y => y.User.Addresses))
                    .FirstOrDefault(x => x.Email == email);
            }
        }

        public ApplicationUser AddAddress(Address address)
        {
            using (var context = new ApplicationDbContext())
            {
                context.Addresses.Add(address);
                context.SaveChanges();

                return GetUserByEmail(this.Email);
            }
        }

        public ApplicationUser AddFriend(Friend friend)
        {
            using (var context = new ApplicationDbContext())
            {
                friend.User = context.Users.FirstOrDefault(x => x.Id == friend.UserId);
                friend.FriendUser = context.Users.FirstOrDefault(x => x.Id == friend.FriendUserId);
                context.Friends.Add(friend);
                context.SaveChanges();

                return GetUserByEmail(this.Email);
            }
        }

        public ApplicationUser ConfirmFriend(string userName)
        {
            using (var context = new ApplicationDbContext())
            {
                var friendTo = context.Friends.FirstOrDefault(x => x.User.UserName == this.UserName && x.FriendUser.UserName == userName);
                var friendFrom = context.Friends.FirstOrDefault(x => x.User.UserName == userName && x.FriendUser.UserName == this.UserName);

                if(friendTo == null && friendFrom == null)
                {
                    var friendUser = context.Users.FirstOrDefault(x => x.UserName == userName).Id;

                    var friend = new Friend()
                    {
                        UserId = this.Id,
                        FriendUserId = friendUser
                    };

                    AddFriend(friend);

                    friend = new Friend()
                    {
                        UserId = this.Id,
                        FriendUserId = friendUser
                    };                    

                    AddFriend(friend);

                    return ConfirmFriend(userName);
                }
                else if (friendTo == null)
                {
                    var friendUser = context.Users.FirstOrDefault(x => x.UserName == userName).Id;

                    var friend = new Friend() {
                        UserId = this.Id,
                        FriendUserId = friendUser
                    };

                    AddFriend(friend);

                    return ConfirmFriend(userName);
                }
                else if (friendFrom == null)
                {
                    var friendUser = context.Users.FirstOrDefault(x => x.UserName == userName).Id;

                    var friend = new Friend()
                    {
                        UserId = friendUser,
                        FriendUserId = this.Id
                    };

                    AddFriend(friend);

                    return ConfirmFriend(userName);
                }

                friendTo.Confirmed = true;
                friendFrom.Confirmed = true;
                context.Entry(friendTo).Property(x => x.Confirmed).IsModified = true;
                context.Entry(friendFrom).Property(x => x.Confirmed).IsModified = true;
                context.SaveChanges();

                return GetUserByEmail(this.Email);
            }
        }

        public ApplicationUser DeleteAddress(long addressid)
        {
            using (var context = new ApplicationDbContext())
            {
                var address = context.Addresses.FirstOrDefault(x => x.AddressId == addressid && x.UserId == this.Id);

                if (address == null)
                {
                    throw new Exception("Attempted to delete Address not owned by User");
                }

                context.Addresses.Remove(address);
                context.SaveChanges();

                return GetUserByEmail(this.Email);
            }
        }

        public ApplicationUser DeleteFriend(string userName)
        {
            using (var context = new ApplicationDbContext())
            {
                var friendId = context.Users.FirstOrDefault(x => x.UserName == userName).Id;

                if (string.IsNullOrWhiteSpace(friendId))
                {
                    throw new Exception("Attempted to delete Friend not owned by User");
                }

                var friendTo = context.Friends.FirstOrDefault(x => x.FriendUserId == friendId);
                var friendFrom = context.Friends.FirstOrDefault(x => x.FriendUserId == this.Id);

                context.Friends.Remove(friendTo);
                if (friendFrom != null)
                {
                    context.Friends.Remove(friendFrom);
                }
                context.SaveChanges();

                return GetUserByEmail(this.Email);
            }
        }

        public ApplicationUser UpdateProfile(UpdateProfileViewModel model)
        {
            using (var context = new ApplicationDbContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Id == this.Id);

                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.PhoneNumber = model.PhoneNumber;
                user.Email = model.Email;

                context.Entry(user).Property(x => x.FirstName).IsModified = true;
                context.Entry(user).Property(x => x.LastName).IsModified = true;
                context.Entry(user).Property(x => x.PhoneNumber).IsModified = true;
                context.Entry(user).Property(x => x.Email).IsModified = true;

                context.SaveChanges();

                return GetUserByEmail(this.Email);
            }
        }

        public ApplicationUser UpdatePassword(string password)
        {
            using (var context = new ApplicationDbContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Id == this.Id);

                user.PasswordHash = password;
                context.Entry(user).Property(x => x.PasswordHash).IsModified = true;
                context.SaveChanges();

                return GetUserByEmail(this.Email);
            }
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Friend> Friends { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>()
                .HasRequired(x => x.User)
                .WithMany(x => x.Addresses)
                .HasForeignKey(x => x.UserId);

            modelBuilder.Entity<Friend>()
                .HasRequired(x => x.User)
                .WithMany(x => x.Friends)
                .HasForeignKey(x => x.UserId);

            modelBuilder.Entity<Friend>()
                .HasRequired(x => x.FriendUser)
                .WithMany(x => x.ExternalFriends)
                .HasForeignKey(x => x.FriendUserId);

            modelBuilder.Entity<Address>()
                .Property(x => x.Longitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Address>()
               .Property(x => x.Latitude)
               .HasPrecision(9, 6);

            base.OnModelCreating(modelBuilder);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}