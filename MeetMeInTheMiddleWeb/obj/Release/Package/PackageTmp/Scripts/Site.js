﻿function pageInitialize() {
    $("input[type=text]").focus(function () {
        $(".navbar-fixed-bottom.visible-xs").removeClass("visible-xs").addClass("hidden");
    });

    $("input[type=text]").blur(function () {
        $(".navbar-fixed-bottom.hidden").addClass("visible-xs").removeClass("hidden");
    });

    $('input[data-val="true"]').each(function () {
        var label = $('label[for="' + $(this).attr('id') + '"]');
        var text = label.text();
        if (text.length > 0) {
            label.append('<span style="color:red"> *</span>');
        }
    });

    $("#PhoneNumber").mask('+0 (000) 000 0000', { clearIfNotMatch: true });
}

pageInitialize();

$("#RegisterForm").submit(function (e) {
    if ($(this).hasClass('values-masked')) {
        e.preventDefault();
        $("#PhoneNumber").unmask();
        $(this).removeClass("values-masked");
        $(this).submit();
    }
});

function loadingScreenStart() {
    $('body').waitMe({
        effect: 'roundBounce',
        text: 'Please Wait.',
        bg: "rgba(255, 255, 255, 0.7)",
        color: "#000"
    });
    var i = 0;
}

function loadingScreenStop() {
    $('body').waitMe('hide');
}