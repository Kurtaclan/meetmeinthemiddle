﻿using MeetMeInTheMiddleWeb.Models;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace MeetMeInTheMiddleWeb.Controllers
{
    public class BaseController : Controller
    {
        public ApplicationUser applicationUser
        {
            get
            {
                if (Session["user"] == null && !string.IsNullOrWhiteSpace(User.Identity.Name))
                {
                    var user = new ApplicationUser().GetUserByUserName(User.Identity.Name);
                    user.Friends = user.Friends ?? new List<Friend>();
                    user.Addresses = user.Addresses ?? new List<Address>();
                    Session["user"] = user;
                }

                return Session["user"] as ApplicationUser;
            }

            set
            {
                Session["user"] = value;
            }
        }

        public bool ValidateEmail(string email)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            return regex.IsMatch(email);
        }

        public bool ValidatePhoneNumber(string phone)
        {
            Regex regex = new Regex(@"^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$");
            return regex.IsMatch(phone);
        }
    }
}