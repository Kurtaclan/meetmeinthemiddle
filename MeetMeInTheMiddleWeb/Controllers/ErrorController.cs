﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetMeInTheMiddleWeb.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [ActionName("404")]
        public ActionResult Error404(string aspxerrorpath)
        {
            return View(model: aspxerrorpath);
        }
    }
}