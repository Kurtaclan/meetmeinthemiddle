namespace MeetMeInTheMiddleWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddConfirmationToFriends : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Friends", "Confirmed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Friends", "Confirmed");
        }
    }
}
