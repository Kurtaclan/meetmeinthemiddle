namespace MeetMeInTheMiddleWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReAddIndexesForAddressAndFriend : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Friend", "UserIdIndex");
            DropIndex("dbo.Friend", "FriendUserIdIndex");
            CreateIndex("dbo.Friend", new string[] { "UserId" },
                false, "UserIdIndex");
            CreateIndex("dbo.Friend", new string[] { "FriendUserId" },
                false, "FriendUserIdIndex");
        }
        
        public override void Down()
        {
        }
    }
}
