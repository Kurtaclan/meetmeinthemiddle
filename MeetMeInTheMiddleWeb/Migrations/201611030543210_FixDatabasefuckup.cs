namespace MeetMeInTheMiddleWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixDatabasefuckup : DbMigration
    {
        public override void Up()
        {
            DropTable("Address");
            DropTable("Friend");
            DropForeignKey("Friends", "ApplicationUser_Id");
        }
        
        public override void Down()
        {
        }
    }
}
