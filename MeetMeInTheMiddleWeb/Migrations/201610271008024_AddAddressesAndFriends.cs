namespace MeetMeInTheMiddleWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddAddressesAndFriends : DbMigration
    {
        public override void Up()
        {
            CreateTable(
               "dbo.Address",
               c => new
               {
                   AddressId = c.Long(identity: true),
                   Name = c.String(nullable: false, maxLength: 256),
                   Location = c.String(nullable: false, maxLength: 256),
                   Latitude = c.Decimal(),
                   Longitude = c.Decimal(),
                   UserId = c.String(nullable: false, maxLength: 128)
               })
               .PrimaryKey(t => t.AddressId)
               .Index(t => t.UserId, unique: false, name: "UserIdIndex");

            CreateTable(
               "dbo.Friend",
               c => new
               {
                   FriendId = c.Long(identity: true),
                   UserId = c.String(nullable: false, maxLength: 128),
                   FriendUserId = c.String(nullable: false, maxLength: 128),
               })
               .PrimaryKey(t => t.FriendId)
               .Index(t => t.UserId, unique: true, name: "UserIdIndex")
               .Index(t => t.FriendUserId, unique: true, name: "FriendUserIdIndex");
        }

        public override void Down()
        {
        }
    }
}
