namespace MeetMeInTheMiddleWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class FixFriendColumns : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Friends", "FK_dbo.Friends_dbo.AspNetUsers_ApplicationUser_Id");
            DropForeignKey("Friends", "FK_dbo.Friends_dbo.AspNetUsers_FriendUser_Id");
            DropForeignKey("Friends", "FK_dbo.Friends_dbo.AspNetUsers_User_Id");

            DropIndex("Friends", "IX_FriendUser_Id");
            DropIndex("Friends", "IX_User_Id");
            DropIndex("Friends", "IX_ApplicationUser_Id");

            DropColumn("Friends", "FriendUser_Id");
            DropColumn("Friends", "User_Id");
            DropColumn("Friends", "ApplicationUser_Id");

            AlterColumn("Friends", "UserId", x => x.String(false, 128));
            AlterColumn("Friends", "FriendUserId", x => x.String(false, 128));
            CreateIndex("Friends", "UserId", false, "IX_UserId");
            CreateIndex("Friends", "FriendUserId", false, "IX_FriendUserId");
            AddForeignKey("Friends", "UserId", "AspNetUsers", "Id", false, "FK_User_To_User");
            AddForeignKey("Friends", "FriendUserId", "AspNetUsers", "Id", false, "FK_FriendUser_To_User");
        }

        public override void Down()
        {
        }
    }
}
