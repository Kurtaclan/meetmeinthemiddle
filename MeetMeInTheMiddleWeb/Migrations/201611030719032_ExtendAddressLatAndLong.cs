namespace MeetMeInTheMiddleWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class ExtendAddressLatAndLong : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Addresses", "Latitude", x => x.Decimal(precision: 30, scale: 20));
            AlterColumn("Addresses", "Longitude", x => x.Decimal(precision: 30, scale: 20));
        }

        public override void Down()
        {
        }
    }
}
