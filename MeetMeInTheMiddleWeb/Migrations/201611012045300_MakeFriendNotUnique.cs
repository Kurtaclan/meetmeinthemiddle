namespace MeetMeInTheMiddleWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class MakeFriendNotUnique : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("dbo.Address", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Friend", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Friend", "FriendUserId", "dbo.AspNetUsers", "Id");
        }

        public override void Down()
        {

        }
    }
}
