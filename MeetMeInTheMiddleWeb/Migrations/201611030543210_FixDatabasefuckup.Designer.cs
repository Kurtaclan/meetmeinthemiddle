// <auto-generated />
namespace MeetMeInTheMiddleWeb.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class FixDatabasefuckup : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(FixDatabasefuckup));
        
        string IMigrationMetadata.Id
        {
            get { return "201611030543210_FixDatabasefuckup"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
