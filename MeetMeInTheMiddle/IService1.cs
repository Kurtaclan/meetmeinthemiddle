﻿using Models;
using System.ServiceModel;

namespace MeetMeInTheMiddle
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
        //BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "/api/GetSearchResults")]
        YelpResult[] GetSearchResults(YelpQuery Query);

        [OperationContract]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
        //BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "/api/SendEmailsAndPhoneNumbers")]
        bool SendEmailsAndPhoneNumbers(string[] ContactData, string Message);

        [OperationContract]
        string[] GetTriangulationBox(GeoLocation[] Locations);

        [OperationContract]
        string Login(string UserName, string Password);

        [OperationContract]
        Contact RetrieveProfile(string UserName);

        [OperationContract]
        Contact[] RetrieveContacts(string UserName);
    }
}
