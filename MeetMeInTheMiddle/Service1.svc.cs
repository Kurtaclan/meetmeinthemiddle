﻿using Models;
using System.Linq;
using System;
using System.IO;
using System.Net;
using System.Text;
using SimpleOAuth;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace MeetMeInTheMiddle
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        static string URI = "https://api.yelp.com/v2/search/";
        static string CONSUMER_KEY = "dI1PJBWremFdBhhnnGUmqw";
        static string CONSUMER_SECRET = "I5cbkOcvcAe7i6k8sCQNo16LHn0";
        static string TOKEN = "JojR6irtUVFcN0-It2QfOwPwWGz-uqBw";
        static string TOKEN_SECRET = "uQrFJLoCq7ohe3320bhIJT0j2Lw";
        static string[] SMS_GATEWAYS = { "@txt.att.net", "@tmomail.net", "@vtext.com", "@pm.sprint.com", "@vmobl.com",
            "@mmst5.tracfone.com", "@myboostmobile.com","@sms.mycricket.com",
            "@ptel.com","@text.republicwireless.com","@tms.suncom.com","@message.ting.com","@email.uscc.net",
            "@cingularme.com","@cspire1.com" };

        public YelpResult[] GetSearchResults(YelpQuery Query)
        {
            var query = System.Web.HttpUtility.ParseQueryString(String.Empty);
            query["term"] = Query.Term;
            query["limit"] = "20";
            query["radius_filter"] = "805";
            query["sort"] = "1";

            if (!string.IsNullOrWhiteSpace(Query.Rating))
            {
                query["rating"] = Query.Rating;
            }

            if (!string.IsNullOrWhiteSpace(Query.Price))
            {
                query["price"] = Query.Price;
            }

            if (!string.IsNullOrWhiteSpace(Query.Categories))
            {
                query["categories"] = Query.Categories;
            }

            if (!string.IsNullOrWhiteSpace(Query.SW_Latitude))
            {
                query["bounds"] = string.Format("{0},{1}|{2},{3}", Query.SW_Latitude, Query.SW_Longitude, Query.NE_Latitude, Query.NE_Longitude);
            }
            else //DEBUG
            {
                query["location"] = "21350 Casino Ridge Rd, Yorba Linda, CA 92887";
            }

            try
            {
                var uriBuilder = new UriBuilder(URI);
                uriBuilder.Query = query.ToString().Replace("%2c", ",").Replace("%7c", "|");

                var request = WebRequest.Create(uriBuilder.ToString());
                request.Method = "GET";

                request.SignRequest(new Tokens
                {
                    ConsumerKey = CONSUMER_KEY,
                    ConsumerSecret = CONSUMER_SECRET,
                    AccessToken = TOKEN,
                    AccessTokenSecret = TOKEN_SECRET
                }).WithEncryption(EncryptionMethod.HMACSHA1).InHeader();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                var stream = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                return JsonConvert.DeserializeObject<YelpSearchResult>(stream.ReadToEnd()).GetYelpResults();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return new YelpResult[0];
        }

        public bool SendEmailsAndPhoneNumbers(string[] ContactData, string Message)
        {
            try
            {
                var emails = ContactData.Where(x => ValidateEmail(x));
                if (emails.Count() > 0)
                {
                    Parallel.ForEach(emails, (email =>
                    {
                        var fromAddress = new MailAddress("meetmeinthemiddleteam@gmail.com", "Meet Me In The Middle Team");
                        var toAddress = new MailAddress(email);
                        const string fromPassword = "afslkja124sadfk";
                        const string subject = "New Meetup from: Meet Me In The Middle";
                        string body = Message;

                        var smtp = new SmtpClient
                        {
                            Host = "smtp.gmail.com",
                            Port = 587,
                            EnableSsl = true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = false,
                            Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                        };
                        using (var message = new MailMessage(fromAddress, toAddress)
                        {
                            Subject = subject,
                            Body = body
                        })
                        {
                            smtp.Send(message);
                        }
                    }));
                }

                var phoneNumbers = from a in ContactData
                                   from b in SMS_GATEWAYS
                                   where ValidatePhoneNumber(a)
                                   select a + b;

                if (phoneNumbers.Count() > 0)
                {
                    Parallel.ForEach(phoneNumbers, (email =>
                    {
                        var fromAddress = new MailAddress("meetmeinthemiddleteam@gmail.com", "Meet Me In The Middle Team");
                        var toAddress = new MailAddress(email);
                        const string fromPassword = "afslkja124sadfk";
                        const string subject = "New Meetup from: Meet Me In The Middle";
                        string body = Message;

                        var smtp = new SmtpClient
                        {
                            Host = "smtp.gmail.com",
                            Port = 587,
                            EnableSsl = true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = false,
                            Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                        };
                        try
                        {
                            using (var message = new MailMessage(fromAddress, toAddress)
                            {
                                Subject = subject,
                                Body = body
                            })
                            {
                                smtp.Send(message);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }));
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string[] GetTriangulationBox(GeoLocation[] Locations)
        {
            Decimal radius = .1M;  //need a better way to determine radius value
            var Longitudes = Locations.Select(x => x.Longitude).ToArray();
            var Latitudes = Locations.Select(x => x.Latitude).ToArray();
            Decimal avgLongitude = Longitudes.Sum() / Longitudes.Length;
            Decimal avgLatitude = Latitudes.Sum() / Latitudes.Length;
            Decimal radiusModifier = radius / Convert.ToDecimal(Math.Sqrt(2));

            string SW_Latitude = Convert.ToString(avgLatitude - radiusModifier);
            string SW_Longitude = Convert.ToString(avgLongitude - radiusModifier);
            string NE_Latitude = Convert.ToString(avgLatitude + radiusModifier);
            string NE_Longitude = Convert.ToString(avgLongitude + radiusModifier);

            return new string[] { SW_Latitude, SW_Longitude, NE_Latitude, NE_Longitude };
        }

        public string Login(string UserName, string Password)
        {
            return "";
        }

        public Contact RetrieveProfile(string UserName)
        {
            return new Contact();
        }

        public Contact[] RetrieveContacts(string UserName)
        {
            return new Contact[0];
        }

        private bool ValidateEmail(string email)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            return regex.IsMatch(email);
        }

        private bool ValidatePhoneNumber(string phone)
        {
            Regex regex = new Regex(@"^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$");
            return regex.IsMatch(phone);
        }
    }
}
